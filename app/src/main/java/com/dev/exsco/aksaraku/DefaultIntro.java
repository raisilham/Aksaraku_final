package com.dev.exsco.aksaraku;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.Toast;


import com.github.paolorotolo.appintro.AppIntro;


public final class DefaultIntro extends AppIntro {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addSlide(SampleSlide.newInstance(R.layout.intro1));
        addSlide(SampleSlide.newInstance(R.layout.intro2));
        addSlide(SampleSlide.newInstance(R.layout.intro3));
        addSlide(SampleSlide.newInstance(R.layout.intro4));
        showStatusBar(false);
        setDepthAnimation();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        Intent home = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(home);
        Toast.makeText(getApplicationContext(), "Selamat Belajar", Toast.LENGTH_LONG).show();
        super.onDonePressed(currentFragment);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        Intent home = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(home);
        Toast.makeText(getApplicationContext(), "Selamat Belajar", Toast.LENGTH_LONG).show();
        super.onSkipPressed(currentFragment);
    }
}

