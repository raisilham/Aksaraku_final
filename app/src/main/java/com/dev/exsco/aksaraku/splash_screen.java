package com.dev.exsco.aksaraku;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.view.Window;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class splash_screen extends AppCompatActivity {
    ConstraintLayout C2;
    ImageView C1;
    ProgressBar P1;
    Animation downtoup, uptodown;
    MediaPlayer soundtrack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        C1 = findViewById(R.id.logo);
        C2 = findViewById(R.id.tambahan);
        P1 = findViewById(R.id.progressbar);
        uptodown = AnimationUtils.loadAnimation(this, R.anim.uptodown);
        downtoup = AnimationUtils.loadAnimation(this, R.anim.downtoup);
        P1.setAnimation(uptodown);
        C1.setAnimation(uptodown);
        C2.setAnimation(downtoup);
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        int SPLASH_TIME_OUT = 5700;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(splash_screen.this, MainActivity.class);
                startActivity(i);
                Log.d("Splash", "splash dimulai");
                finish();

            }
        }, SPLASH_TIME_OUT);
    }
}
