package com.dev.exsco.aksaraku;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;

import java.util.Objects;

public class sandangan extends AppCompatActivity {
    private int urutan = 1;
    private TextView penjelasan, big, filosofi, panelcontoh, arti, panelcontoh2, arti2;
    private NavigationView navigationView1;
    private DrawerLayout mDrawerLayou1;
    private ActionBarDrawerToggle mToogle1;
    CollapsingToolbarLayout collapsingToolbarLayout1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sandangan);
        penjelasan = findViewById(R.id.penjelasan_ha);
        big = findViewById(R.id.big);
        filosofi = findViewById(R.id.filosofi);
        panelcontoh = findViewById(R.id.panelcontoh);
        arti = findViewById(R.id.arti);
        Bundle extras = getIntent().getExtras();
        urutan = Objects.requireNonNull(extras).getInt("aksara");
        aksaraSelector(urutan);
        Boolean firstLoad = getSharedPreferences("sandangan", MODE_PRIVATE)
                .getBoolean("sandangan1", true);
        if (firstLoad) {
            TapTargetView.showFor(this,
                    TapTarget.forView(findViewById(R.id.fab), "Tombol ini untuk melihat Aksara asli", "Untuk melihat Aksara asli dari sandangan")
                            .tintTarget(false)
                            .outerCircleColor(R.color.slide1)
                            .textColor(R.color.white)
                            .cancelable(false));
            TapTargetView.showFor(this,
                    TapTarget.forView(findViewById(R.id.fabsuara), "Tombol ini untuk Mendengar", "Untuk mendengarkan bunyi")
                            .tintTarget(false)
                            .outerCircleColor(R.color.slide3)
                            .textColor(R.color.white)
                            .cancelable(false));


            getSharedPreferences("sandangan", MODE_PRIVATE).edit().putBoolean("sandangan1", false).apply();
        }

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        panelcontoh2 = findViewById(R.id.panelcontoh22);
        arti2 = findViewById(R.id.arti22);
        navigationView1 = findViewById(R.id.navigation);
        mDrawerLayou1 = findViewById(R.id.drawer_layout1);
        mToogle1 = new ActionBarDrawerToggle(this, mDrawerLayou1, toolbar, R.string.open, R.string.close);
        mDrawerLayou1.addDrawerListener(mToogle1);
        mDrawerLayou1.post(new Runnable() {
            @Override
            public void run() {
                mToogle1.syncState();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        collapsingToolbarLayout1 = findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout1.setTitle("Menengah");
        collapsingToolbarLayout1.setExpandedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout1.setCollapsedTitleTextColor(
                ContextCompat.getColor(this, R.color.white));
        collapsingToolbarLayout1.setExpandedTitleColor(
                ContextCompat.getColor(this, R.color.white));
        navigationView1.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.sejarah:
                        Intent i = new Intent(getApplicationContext(), sejarahh.class);
                        startActivity(i);
                        break;
                    case R.id.dasar:
                        Intent r = new Intent(getApplicationContext(), dasar.class);
                        startActivity(r);
                        break;
                    case R.id.menengah:
                        Intent z = new Intent(getApplicationContext(), menengah.class);
                        startActivity(z);
                        break;
                    case R.id.lanjutan:
                        Intent t = new Intent(getApplicationContext(), lanjutan.class);
                        startActivity(t);
                        break;
                    case R.id.tebak:
                        Intent u = new Intent(getApplicationContext(), dashboard_lvltebak.class);
                        startActivity(u);
                        break;
                    case R.id.terjemah:
                        Intent w = new Intent(getApplicationContext(), dasboardterjemah.class);
                        startActivity(w);
                        break;
                    case R.id.about:
                        Intent ab = new Intent(getApplicationContext(), about.class);
                        startActivity(ab);
                        break;
                }
                return false;
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (mDrawerLayou1.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayou1.closeDrawer(GravityCompat.START);
        } else {
            Intent home = new Intent(getApplicationContext(), menengah.class);
            startActivity(home);
            super.onBackPressed();

        }

    }

    private void aksaraSelector(int selector) {
        switch (selector) {
            case 1:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("H");
                penjelasan.setText(R.string.pha);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("ankHnk\\");
                arti.setText("Anak anak");
                FloatingActionButton fab = findViewById(R.id.fab);
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 1);
                        startActivity(i);
                    }
                });
                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("a[rkH[rk\\");
                arti2.setText("Arek arek");
                FloatingActionButton fab110 = findViewById(R.id.fabsuara);
                fab110.bringToFront();
                fab110.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ha1);
                        suarabenar.start();
                    }
                });


                break;
            case 2:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("N");
                penjelasan.setText(R.string.pna);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("nklNkl\\");
                arti.setText("Nakal nakal");
                FloatingActionButton fab1 = findViewById(R.id.fab);
                fab1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 2);
                        startActivity(i);
                    }
                });

                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("nnemNns\\");
                arti2.setText("Nanem Nanas");
                FloatingActionButton fab111 = findViewById(R.id.fabsuara);
                fab111.bringToFront();
                fab111.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.na);
                        suarabenar.start();
                    }
                });
                break;
            case 3:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("C");
                penjelasan.setText(R.string.pca);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("c[lonCmt\\");
                arti.setText("Calon Camat");

                FloatingActionButton fab2 = findViewById(R.id.fab);
                fab2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 3);
                        startActivity(i);
                    }
                });
                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("cepkCepk\\");
                arti2.setText("Cepak cepak");

                FloatingActionButton fab112 = findViewById(R.id.fabsuara);
                fab112.bringToFront();
                fab112.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ca);
                        suarabenar.start();
                    }
                });
                break;

            case 4:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("R");
                penjelasan.setText(R.string.pra);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("rgfRbi");
                arti.setText("ragad rabi");
                FloatingActionButton fab3 = findViewById(R.id.fab);
                fab3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 4);
                        startActivity(i);
                    }
                });
                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("rckRck\\");
                arti2.setText("Racak racak");
                FloatingActionButton fab113 = findViewById(R.id.fabsuara);
                fab113.bringToFront();
                fab113.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ra);
                        suarabenar.start();
                    }
                });
                break;
            case 5:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("K");
                penjelasan.setText(R.string.pka);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("kpukKps\\");
                arti.setText("Kapuk kapas");
                FloatingActionButton fab4 = findViewById(R.id.fab);
                fab4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 5);
                        startActivity(i);
                    }
                });
                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("kwnKwn\\");
                arti2.setText("Kawan kawan");
                FloatingActionButton fab114 = findViewById(R.id.fabsuara);
                fab114.bringToFront();
                fab114.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ka);
                        suarabenar.start();
                    }
                });
                break;
            case 6:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("F");
                penjelasan.setText(R.string.pda);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("f[fosF[lm\\");
                arti.setText("Dados dalem");
                FloatingActionButton fab5 = findViewById(R.id.fab);
                fab5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 6);
                        startActivity(i);
                    }
                });

                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("flnFln\\");
                arti2.setText("Dalan dalan");
                FloatingActionButton fab115 = findViewById(R.id.fabsuara);
                fab115.bringToFront();
                fab115.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.da);
                        suarabenar.start();
                    }
                });
                break;
            case 7:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("T");
                penjelasan.setText(R.string.pta);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("tpkTils\\");
                arti.setText("tapak tilas");
                FloatingActionButton fab6 = findViewById(R.id.fab);
                fab6.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 7);
                        startActivity(i);
                    }
                });

                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("amPsTau");
                arti2.setText("Ampas tahu");
                FloatingActionButton fab116 = findViewById(R.id.fabsuara);
                fab116.bringToFront();
                fab116.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("text", "trst");
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ta);
                        suarabenar.start();
                    }
                });

                break;
            case 8:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("S");
                penjelasan.setText(R.string.pta);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("snkSefulu/");
                arti.setText("Sanak sedulur");
                FloatingActionButton fab7 = findViewById(R.id.fab);
                fab7.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 8);
                        startActivity(i);
                    }
                });

                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("sbenSsi");
                arti2.setText("Saben sasi");
                FloatingActionButton fab117 = findViewById(R.id.fabsuara);
                fab117.bringToFront();
                fab117.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.sa);
                        suarabenar.start();
                    }
                });
                break;
            case 9:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek \n ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("W");
                penjelasan.setText(R.string.pwa);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("wtukWtuk\\");
                arti.setText("Watuk watuk");
                FloatingActionButton fab8 = findViewById(R.id.fab);
                fab8.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 9);
                        startActivity(i);
                    }
                });
                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("wsisWicr");
                arti2.setText("Wasis wicara");
                FloatingActionButton fab118 = findViewById(R.id.fabsuara);
                fab118.bringToFront();
                fab118.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.wa);
                        suarabenar.start();
                    }
                });
                FloatingActionButton fab1181 = findViewById(R.id.fabsuara);
                fab1181.bringToFront();
                fab1181.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.wa);
                        suarabenar.start();
                    }
                });

                break;
            case 10:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("L");
                penjelasan.setText(R.string.pla);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("lmtLmt\\");
                arti.setText("Lamat Lamat");
                FloatingActionButton fab9 = findViewById(R.id.fab);
                fab9.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 10);
                        startActivity(i);
                    }
                });
                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("vw=LmPu");
                arti2.setText("Nyawang lampu");
                FloatingActionButton fab119 = findViewById(R.id.fabsuara);
                fab119.bringToFront();
                fab119.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.la);
                        suarabenar.start();
                    }
                });
                break;
            case 11:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("P");
                penjelasan.setText(R.string.ppa);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("p[nnPfi");
                arti.setText("Panen Padi");
                FloatingActionButton fab10 = findViewById(R.id.fab);
                fab10.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 11);
                        startActivity(i);
                    }
                });


                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("p[nCnPns\\");
                arti2.setText("Pancen panas");
                FloatingActionButton fab120 = findViewById(R.id.fabsuara);
                fab120.bringToFront();
                fab120.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.pa);
                        suarabenar.start();
                    }
                });
                break;
            case 12:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("D");
                penjelasan.setText(R.string.pdha);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("dnD=");
                arti.setText("dhandhang");
                FloatingActionButton fab11 = findViewById(R.id.fab);
                fab11.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 12);
                        startActivity(i);
                    }
                });

                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("dwulDwul\\");
                arti2.setText("Dhawul dhawul");
                FloatingActionButton fab121 = findViewById(R.id.fabsuara);
                fab121.bringToFront();
                fab121.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.dha);
                        suarabenar.start();
                    }
                });
                break;
            case 13:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("J");
                penjelasan.setText(R.string.pja);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("nJjl\\");
                arti.setText("Njajal");
                FloatingActionButton fab12 = findViewById(R.id.fab);
                fab12.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 13);
                        startActivity(i);
                    }
                });
                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("j=gelJgu=");
                arti2.setText("Janggel jagung");
                FloatingActionButton fab122 = findViewById(R.id.fabsuara);
                fab122.bringToFront();
                fab122.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ja);
                        suarabenar.start();
                    }
                });
                break;
            case 14:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("Y");
                penjelasan.setText(R.string.pya);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("ykinYekti");
                arti.setText("Yakin yekti");
                FloatingActionButton fab13 = findViewById(R.id.fab);
                fab13.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 14);
                        startActivity(i);
                    }
                });
                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("tennYkin\\");
                arti2.setText("tenan yakin");
                FloatingActionButton fab123 = findViewById(R.id.fabsuara);
                fab123.bringToFront();
                fab123.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ya);
                        suarabenar.start();
                    }
                });
                break;
            case 15:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("V");
                penjelasan.setText(R.string.pnya);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("vbutVw");
                arti.setText("Nyabut nyawa");
                FloatingActionButton fab14 = findViewById(R.id.fab);
                fab14.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 15);
                        startActivity(i);
                    }
                });
                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("vmukVmuk\\");
                arti2.setText("Nyamuk nyamuk");
                FloatingActionButton fab124 = findViewById(R.id.fabsuara);
                fab124.bringToFront();
                fab124.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.nya);
                        suarabenar.start();
                    }
                });
                break;
            case 16:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("M");
                penjelasan.setText(R.string.pma);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("m=gutM=gut\\");
                arti.setText("manggut manggut");
                FloatingActionButton fab15 = findViewById(R.id.fab);
                fab15.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 16);
                        startActivity(i);
                    }
                });
                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("mnukMv/");
                arti2.setText("Manuk manyar");
                FloatingActionButton fab126 = findViewById(R.id.fabsuara);
                fab126.bringToFront();
                fab126.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ma);
                        suarabenar.start();
                    }
                });
                break;
            case 17:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("G");
                penjelasan.setText(R.string.pga);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("ggkGlk\\");
                arti.setText("Gagak galak");
                FloatingActionButton fab16 = findViewById(R.id.fab);
                fab16.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 17);
                        startActivity(i);
                    }
                });
                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("aiw[kGo[s=o");
                arti2.setText("Iwak gosong");
                FloatingActionButton fab127 = findViewById(R.id.fabsuara);
                fab127.bringToFront();
                fab127.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ga);
                        suarabenar.start();
                    }
                });
                break;
            case 18:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("B");
                penjelasan.setText(R.string.pba);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("blBlLn\\");
                arti.setText("Bal ballan");
                FloatingActionButton fab17 = findViewById(R.id.fab);
                fab17.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 18);
                        startActivity(i);
                    }
                });
                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("bkulBtik\\");
                arti2.setText("Bakul batik");
                FloatingActionButton fab128 = findViewById(R.id.fabsuara);
                fab128.bringToFront();
                fab128.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ba);
                        suarabenar.start();
                    }
                });
                break;
            case 19:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("Q");
                penjelasan.setText(R.string.ptha);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("kLenQi=");
                arti.setText("Klenthing");
                FloatingActionButton fab18 = findViewById(R.id.fab);
                fab18.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 19);
                        startActivity(i);
                    }
                });
                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("ge[nQ=o");
                arti2.setText("Genthong");
                FloatingActionButton fab129 = findViewById(R.id.fabsuara);
                fab129.bringToFront();
                fab129.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.tha);
                        suarabenar.start();
                    }
                });
                break;
            case 20:
                filosofi.setText("pasangan aksara jawa berfungsi untuk menghubungkan suku kata tertutup konsonan dengan suku kata berikutnya\n\n" +
                        "Kecuali suku kata tertutup antara lain Wignyan, Layar, dan cecek ( ada di materi lanjutan )");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("Z");
                penjelasan.setText(R.string.pnga);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("zjkZ[so");
                arti.setText("Ngajak Ngaso");
                FloatingActionButton fab19 = findViewById(R.id.fab);
                fab19.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(sandangan.this, ha.class);
                        i.putExtra("aksara", 20);
                        startActivity(i);
                    }
                });
                panelcontoh2 = findViewById(R.id.panelcontoh22);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti22);
                panelcontoh2.setText("zjkZji");
                arti2.setText("Ngajak ngaji");
                FloatingActionButton fab130 = findViewById(R.id.fabsuara);
                fab130.bringToFront();
                fab130.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.nga);
                        suarabenar.start();
                    }
                });
                break;
        }

    }
}
