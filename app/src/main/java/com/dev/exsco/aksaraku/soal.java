package com.dev.exsco.aksaraku;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class soal extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    public soal() {

    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_soal, container, false);

//        CardView dengar = view.findViewById(R.id.dengar);
        CardView terjemah = view.findViewById(R.id.terjemah);
        CardView tebak = view.findViewById(R.id.tebak);
        terjemah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent terjemah = new Intent(getActivity(), dasboardterjemah.class);
                startActivity(terjemah);
            }
        });
        tebak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tebak = new Intent(getActivity(), dashboard_lvltebak.class);
                startActivity(tebak);
            }
        });
        return view;

    }
}

