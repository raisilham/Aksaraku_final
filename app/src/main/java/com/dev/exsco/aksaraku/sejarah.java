package com.dev.exsco.aksaraku;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;


public class sejarah extends AppCompatActivity {
    private final String API_KEY = "AIzaSyDd2yLefMRCkiOfhwjx9YqSPQpz2xO9Q54";
    private final String VID_CODE = "BzeWJbfQotg";
    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mToogle;
    Toolbar toolbar;
    TextView view, data, data1;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sejarah);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        navigationView = findViewById(R.id.navigation);


        TextView view = findViewById(R.id.testfont);
        view.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        view.setText("a n c r k");
        TextView data = findViewById(R.id.aksara2);
        data.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        data.setText("f t s w l");
        TextView data1 = findViewById(R.id.aksara3);
        data1.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        data1.setText("p d j y v");
        TextView data2 = findViewById(R.id.aksara4);
        data2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        data2.setText("m g b q z");


//        Button button = findViewById(R.id.buttonn);
//        Button youtube1 = findViewById(R.id.buttonnn);
//        youtube1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent coba = new Intent(sejarah.this, youtube.class);
//                startActivity(coba);
//                Toast.makeText(sejarah.this, "Bismillah", Toast.LENGTH_LONG).show();
//            }
//        });
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String videoId = "BzeWJbfQotg";
//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:"+videoId));
//                intent.putExtra("VIDEO_ID", videoId);
//                startActivity(intent);
//            }
//        });


        mDrawerLayout = findViewById(R.id.drawer_layout);
        mToogle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToogle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mToogle.syncState();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle("Sejarah");
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextColor(
                ContextCompat.getColor(this, R.color.white));
        collapsingToolbarLayout.setExpandedTitleColor(
                ContextCompat.getColor(this, R.color.white));

//      collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
// untuk mengubah ukuran teks
//
//      untuk menganti warna saat transisi coapsing gunakan @style dan uban primary Color


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.sejarah:
                        Toast.makeText(getApplicationContext(), "Anda sedang berada di halaman sejarah", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.dasar:
                        Intent r = new Intent(sejarah.this, dasar.class);
                        startActivity(r);
                        break;
                    case R.id.menengah:
                        Intent s = new Intent(sejarah.this, menengah.class);
                        startActivity(s);
                        break;
                    case R.id.lanjutan:
                        Intent t = new Intent(sejarah.this, lanjutan.class);
                        startActivity(t);
                        break;
                    case R.id.tebak:
                        Intent u = new Intent(sejarah.this, tebak.class);
                        startActivity(u);
                        break;
                    case R.id.terjemah:
//                        Intent w = new Intent(sejarah.this, terjemah.class);
//                        startActivity(w);
                        break;
                }
                return false;
            }
        });
    }

    //
    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            Intent home = new Intent(sejarah.this, MainActivity.class);
            startActivity(home);
            super.onBackPressed();

        }
    }
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            finish();
//        }
//        return super.onKeyDown(keyCode, event);
//    }

}
