package com.dev.exsco.aksaraku;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;

import java.util.Objects;

public class isilanjutan extends AppCompatActivity {
    private int urutan = 1;
    private TextView penjelasan, big, filosofi, panelcontoh, arti, penting, panelcontoh2, arti2, contoh;
    private NavigationView navigationView1;
    private DrawerLayout mDrawerLayou1;
    private ActionBarDrawerToggle mToogle1;
    CollapsingToolbarLayout collapsingToolbarLayout1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isilanjutan);
        penjelasan = findViewById(R.id.penjelasan_ha);
        big = findViewById(R.id.big);
        filosofi = findViewById(R.id.filosofi);
        panelcontoh = findViewById(R.id.panelcontoh);
        arti = findViewById(R.id.arti);
        panelcontoh2 = findViewById(R.id.panelcontoh2);
        arti2 = findViewById(R.id.arti2);
        contoh = findViewById(R.id.contoh);
        navigationView1 = findViewById(R.id.navigation);
        Bundle extras = getIntent().getExtras();
        urutan = Objects.requireNonNull(extras).getInt("aksara");
        aksaraSelector(urutan);
        ImageView imageView = findViewById(R.id.gambar);


        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        mDrawerLayou1 = findViewById(R.id.drawer_layout1);
        mToogle1 = new ActionBarDrawerToggle(this, mDrawerLayou1, toolbar, R.string.open, R.string.close);
        mDrawerLayou1.addDrawerListener(mToogle1);
        mDrawerLayou1.post(new Runnable() {
            @Override
            public void run() {
                mToogle1.syncState();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        collapsingToolbarLayout1 = findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout1.setTitle("Lanjutan");
        collapsingToolbarLayout1.setExpandedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout1.setCollapsedTitleTextColor(
                ContextCompat.getColor(this, R.color.white));
        collapsingToolbarLayout1.setExpandedTitleColor(
                ContextCompat.getColor(this, R.color.white));
        navigationView1.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.sejarah:
                        Intent i = new Intent(getApplicationContext(), sejarahh.class);
                        startActivity(i);
                        break;
                    case R.id.dasar:
                        Intent r = new Intent(getApplicationContext(), dasar.class);
                        startActivity(r);
                        break;
                    case R.id.menengah:
                        Intent s = new Intent(getApplicationContext(), menengah.class);
                        startActivity(s);
                        break;
                    case R.id.lanjutan:
                        Intent z = new Intent(getApplicationContext(), lanjutan.class);
                        startActivity(z);
                        break;
                    case R.id.tebak:
                        Intent u = new Intent(getApplicationContext(), dashboard_lvltebak.class);
                        startActivity(u);
                        break;
                    case R.id.terjemah:
                        Intent w = new Intent(getApplicationContext(), dasboardterjemah.class);
                        startActivity(w);
                        break;
                    case R.id.about:
                        Intent ab = new Intent(getApplicationContext(), about.class);
                        startActivity(ab);
                        break;

                }
                return false;
            }
        });
    }

    private void aksaraSelector(int selector) {
        switch (selector) {
            case 1:
                filosofi.setText(R.string.taling);
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("[   o");
                penjelasan.setText(R.string.talung);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("[fo[folS[t");
                arti.setText("Dodol sate");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("[boch");
                arti2.setText("Bocah");
                break;
            case 2:
                filosofi.setText(R.string.suku);
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("    u   ");
                penjelasan.setText("suku");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("figugu");
                arti.setText("digugu");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("tukubuku");
                arti2.setText("Tuku buku");
                break;
            case 3:
                filosofi.setText(R.string.wulu);
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("  i");
                penjelasan.setText("wulu");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("pipi");
                arti.setText("pipi");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("pribsn\\");
                arti2.setText("paribasan");
                break;
            case 4:
                filosofi.setText("Dipakai untuk melambangkan bunyi vokal \"é\" dalam aksara jawa");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("[");
                penjelasan.setText("Taling");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("[r[n [d[w");
                arti.setText("réné déwé");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("[j[j/");
                arti2.setText("Jéjér");
                TapTargetView.showFor(this,
                        TapTarget.forView(findViewById(R.id.fab1), "Suara", "Tekan tombol suara untuk mendengar penjelasan")
                                .tintTarget(false)
                                .outerCircleColor(R.color.slide1)
                                .textColor(R.color.white)
                                .cancelable(false));
                FloatingActionButton fab31111 = findViewById(R.id.fab1);
                fab31111.setVisibility(View.VISIBLE);
                fab31111.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.lambang);
                        suarabenar.start();
                    }
                });
                break;
            case 5:
                filosofi.setText("Dipakai untuk untuk melambangkan konsanan \"r\" pada akhir kata");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("/");
                penjelasan.setText("Layar");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("ps/");
                arti.setText("Pasar");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("ksu/");
                arti2.setText("kasur");
                break;
            case 6:
                filosofi.setText("dipakai untuk melambangkan konsonan \"ng\" pada akhir kata");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("   =   ");
                penjelasan.setText("Cecek");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("bw=");
                arti.setText("Bawang");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("gri=");
                arti2.setText("garing");
                break;
            case 7:
                filosofi.setText("dipakai untuk melambangkan konsonan \"h\" pada akhir kata");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("h");
                penjelasan.setText("wignyan");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("ggh");
                arti.setText("gagah");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("syh");
                arti2.setText("Sayah");
                break;
            case 8:
                filosofi.setText("Dipakai pada akhir bagian kalimat sebagai tanda intonasi setengah selesai. atau bisa juga disebut tanda koma");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText(",");
                penjelasan.setText("Pada Lingsa");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("[wo= ge[d, duwu/");
                arti.setText("Wong gedhe, dhuwur");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("[roti,lnG|l");
                arti2.setText("Roti, lan Gula");
                break;
            case 9:
                filosofi.setText("Dipakai di akhir kalimat, atau bisa juga disebut tanda titik");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText(".");
                penjelasan.setText("Pada Lungsi");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("suminifuru=turu.");
                arti.setText("Sumini durung turu");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("pikir[n[go[rh.");
                arti2.setText("pikirane goreh");
                break;
            case 10:
                filosofi.setText("Dipakai untuk mengapit angka dan pernyatan yang menggunakan titik dua \":\"");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText(";");
                penjelasan.setText("pada pangkat");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText(";1999;");
                arti.setText("1999");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText(":75:ki[lo");
                arti2.setText("75 kilo");
                break;
            case 11:
                filosofi.setText("merupakan penanda gugus konsonan yang unsur terakhimya berwujud kansanan \"r\" atau berbunyi ra .\n\n" +
                        "Klik tombol audio jika bungung membedakan penggunaan cakra dengan pasangan \"ra\"");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("]");
                penjelasan.setText("Cakra");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("ss]");
                arti.setText("sasra");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("k]m");
                arti2.setText("krama");
                TapTargetView.showFor(this,
                        TapTarget.forView(findViewById(R.id.fab1), "Suara", "Tekan tombol suara untuk mendengar penjelasan")
                                .tintTarget(false)
                                .outerCircleColor(R.color.fab)
                                .textColor(R.color.white)
                                .cancelable(false));
                FloatingActionButton fab10000 = findViewById(R.id.fab1);
                fab10000.setVisibility(View.VISIBLE);
                fab10000.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.cakradanra);
                        suarabenar.start();
                    }
                });

                break;
            case 12:
                filosofi.setText("Dipakai untuk melambangkan gugus konsonan yang berunsur akhir konsonan r yang diikut huruf \"é\"");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("}");
                penjelasan.setText("Cakra keret");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("k}tek\\");
                arti.setText("krétek");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("b}[gos\\");
                arti2.setText("Bréngos");
                break;
            case 13:
                filosofi.setText("Penganti aksara ra di pepet\n" +
                        "Dibaca \"re\"\n" +
                        "Karena ra tidak bisa dipepet");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("x");
                penjelasan.setText("Pa Cerek");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("mxmTenn\\");
                arti.setText("marem tenan");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("xms\\");
                arti2.setText("remas");
                break;
            case 14:
                filosofi.setText("Penganti aksara la di pepet\n" +
                        "Dibaca \"le\"\n" +
                        "Karena la tidak bisa dipepet");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("X");
                penjelasan.setText("Nga lelet");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("Xg");
                arti.setText("lega");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("peXm\\");
                arti2.setText("Pelem");
                break;
            case 15:
                filosofi.setText("Dipakai ditiap tiap awal alinea");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("?");
                penjelasan.setText("Adeg-adeg");
                ImageView imageView = findViewById(R.id.gambar);
                imageView.setVisibility(View.VISIBLE);
//                contoh.setVisibility(View.INVISIBLE);
//                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                break;
            case 16:
                filosofi.setText("Dipakai untuk melambangkan konsonan y yang bergabung dengan konsonan lain didalam suatu suku kata");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("-");
                penjelasan.setText("Pengkal");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("t-s\\");
                arti.setText("tyas");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("kp-/s");
                arti2.setText("kapyarsa");
                break;
            case 17:
                filosofi.setText("Dipakai untuk menuliskan nama gelar, nama diri, nama geografi, nama lembaga pemerintah");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("!");
                penjelasan.setText("Aksara Murda Na");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("!bi!uh");
                arti.setText("Nabi nuh");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("[!/omn");
                arti2.setText("Norman");
                FloatingActionButton fab21 = findViewById(R.id.fab);
                fab21.setVisibility(View.VISIBLE);
                fab21.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), isilanjutan.class);
                        i.putExtra("aksara", 48);
                        startActivity(i);
                    }
                });
                break;
            case 18:
                filosofi.setText("Dipakai untuk menuliskan nama gelar, nama diri, nama geografi, nama lembaga pemerintah");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("@");
                penjelasan.setText("Aksara Murda Ka");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("@ lik]sk\\");
                arti.setText("Kali krasak");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("@ bup[tnBnÒ|l\\");
                arti2.setText("Kabupaten Bantul");
                FloatingActionButton fab2 = findViewById(R.id.fab);
                fab2.setVisibility(View.VISIBLE);
                fab2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), isilanjutan.class);
                        i.putExtra("aksara", 36);
                        startActivity(i);
                    }
                });
                break;
            case 19:
                filosofi.setText("Dipakai untuk menuliskan nama gelar, nama diri, nama geografi, nama lembaga pemerintah");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("#");
                penjelasan.setText("Aksara Murda Ta");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("#w=mzu");
                arti.setText("Tawang mangu");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("[#orsufi[ro");
                arti2.setText("Torasudiro");
                FloatingActionButton fab3 = findViewById(R.id.fab);
                fab3.setVisibility(View.VISIBLE);
                fab3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), isilanjutan.class);
                        i.putExtra("aksara", 49);
                        startActivity(i);
                    }
                });
                break;
            case 20:
                filosofi.setText("Dipakai untuk menuliskan nama gelar, nama diri, nama geografi, nama lembaga pemerintah");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("$");
                penjelasan.setText("Aksara murda Sa");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("$usiwi");
                arti.setText("Susiwi");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("$lSa");
                arti2.setText("Salsa");
                FloatingActionButton fab4 = findViewById(R.id.fab);
                fab4.setVisibility(View.VISIBLE);
                fab4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), isilanjutan.class);
                        i.putExtra("aksara", 50);
                        startActivity(i);
                    }
                });
                break;
            case 21:
                filosofi.setText("Dipakai untuk menuliskan nama gelar, nama diri, nama geografi, nama lembaga pemerintah");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("%");
                penjelasan.setText("Aksara murda Pa");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("%[zrn\\");
                arti.setText("Pangeran");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("%aiMn\\");
                arti2.setText("Paiman");
                FloatingActionButton fab5 = findViewById(R.id.fab);
                fab5.setVisibility(View.VISIBLE);
                fab5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), isilanjutan.class);
                        i.putExtra("aksara", 51);
                        startActivity(i);
                    }
                });
                break;
            case 22:
                filosofi.setText("Dipakai untuk menuliskan nama gelar, nama diri, nama geografi, nama lembaga pemerintah");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("^");
                penjelasan.setText("Aksara murda Nya");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("^i[ro[rokiful\\");
                arti.setText("Nyi Roro Kidul");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("^isiti");
                arti2.setText("Nyi siti");
                FloatingActionButton fab6 = findViewById(R.id.fab);
                fab6.setVisibility(View.VISIBLE);
                fab6.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), isilanjutan.class);
                        i.putExtra("aksara", 52);
                        startActivity(i);
                    }
                });
                break;
            case 23:
                filosofi.setText("Dipakai untuk menuliskan nama gelar, nama diri, nama geografi, nama lembaga pemerintah");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("&");
                penjelasan.setText("Aksara murda Ga");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("&nF|=");
                arti.setText("Gandung");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("&un=umerpi");
                arti2.setText("Gunung merapi");
                FloatingActionButton fab7 = findViewById(R.id.fab);
                fab7.setVisibility(View.VISIBLE);
                fab7.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), isilanjutan.class);
                        i.putExtra("aksara", 53);
                        startActivity(i);
                    }
                });
                break;
            case 24:
                filosofi.setText("Dipakai untuk menuliskan nama gelar, nama diri, nama geografi, nama lembaga pemerintah");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("*");
                penjelasan.setText("Aksara murda Ba");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("*upti");
                arti.setText("Bupati");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("*vuwzi");
                arti2.setText("Banyuwangi");
                FloatingActionButton fab8 = findViewById(R.id.fab);
                fab8.setVisibility(View.VISIBLE);
                fab8.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), isilanjutan.class);
                        i.putExtra("aksara", 54);
                        startActivity(i);
                    }
                });
                break;
            case 25:
                filosofi.setText("Aksara rekaan dipakai untuk menuliskan aksara konsonan pada kata-kata asing yang masih dipertahankan seperti aslinya.\n" +
                        "Untuk lebih jelasnya lihat contoh");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("k+");
                penjelasan.setText("Aksara Rekan Kha");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("k+tib\\");
                arti.setText("Khatib");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("k+utBh");
                arti2.setText("Khutbah");
                break;
            case 26:
                filosofi.setText("Aksara rekaan dipakai untuk menuliskan aksara konsonan pada kata-kata asing yang masih dipertahankan seperti aslinya.\n" +
                        "Untuk lebih jelasnya lihat contoh");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("p+");
                penjelasan.setText("Aksara Rekan F/V");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("p+itmin\\");
                arti.setText("vitamin");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("[p+oli[yo");
                arti2.setText("Folio");
                break;
            case 27:
                filosofi.setText("Aksara rekaan dipakai untuk menuliskan aksara konsonan pada kata-kata asing yang masih dipertahankan seperti aslinya.\n" +
                        "Untuk lebih jelasnya lihat contoh");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("f+");
                penjelasan.setText("Aksara Rekan Dz");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("f+lim\\");
                arti.setText("Dzalim");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("f+iki/");
                arti2.setText("Dzikir");
                break;
            case 28:
                filosofi.setText("Aksara rekaan dipakai untuk menuliskan aksara konsonan pada kata-kata asing yang masih dipertahankan seperti aslinya.\n" +
                        "Untuk lebih jelasnya lihat contoh");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("g+");
                penjelasan.setText("Aksara Rekan Gh");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("g+ulm\\");
                arti.setText("Ghulam");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("g+j+li");
                arti2.setText("Ghazali");
                break;
            case 29:
                filosofi.setText("Aksara rekaan dipakai untuk menuliskan aksara konsonan pada kata-kata asing yang masih dipertahankan seperti aslinya.\n" +
                        "Untuk lebih jelasnya lihat contoh");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("j+");
                penjelasan.setText("Aksara Rekan Z");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("j+kt\\");
                arti.setText("Zakat");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("j+iyrh");
                arti2.setText("Ziarah");
                break;
            case 30:
                filosofi.setText("Aksara suara digunakan untuk menuliskan aksara vokal yang menjadi suku kata. \n" +
                        "terutama yang berasal dari bahasa asing untuk mempenegas pelafalannya.");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("A");
                penjelasan.setText("Aksara Swara A");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("AsH/");
                arti.setText("Ashar");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("AkilBlig\\");
                arti2.setText("Akil Balig");
                break;
            case 31:
                filosofi.setText("Aksara suara digunakan untuk menuliskan aksara vokal yang menjadi suku kata. \n" +
                        "terutama yang berasal dari bahasa asing untuk mempenegas pelafalannya.");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("I");
                penjelasan.setText("Aksara Swara I");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("If+/");
                arti.setText("Idzar");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("Ik+Tiy/");
                arti2.setText("Ikhtiyar");

                break;
            case 32:
                filosofi.setText("Aksara suara digunakan untuk menuliskan aksara vokal yang menjadi suku kata. \n" +
                        "terutama yang berasal dari bahasa asing untuk mempenegas pelafalannya.");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("U");
                penjelasan.setText("Aksara Swara U");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("Uk+wh");
                arti.setText("Ukhuwah");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("Uj+u/");
                arti2.setText("Uzur");
                break;
            case 33:
                filosofi.setText("Aksara suara digunakan untuk menuliskan aksara vokal yang menjadi suku kata. \n" +
                        "terutama yang berasal dari bahasa asing untuk mempenegas pelafalannya.");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("E");
                penjelasan.setText("Aksara Swara E");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("E[rop");
                arti.setText("Eropa");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("EsKi[mo");
                arti2.setText("Eskimo");
                break;
            case 34:
                filosofi.setText("Aksara suara digunakan untuk menuliskan aksara vokal yang menjadi suku kata. \n" +
                        "terutama yang berasal dari bahasa asing untuk mempenegas pelafalannya.");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("O");
                penjelasan.setText("Aksara Swara O");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("O/gn");
                arti.setText("Organ");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("O/kest]");
                arti2.setText("Orkestra");
                break;
            case 35:
                filosofi.setText("Dipakai sebagai penanda bahwa aksara yang dibubuhi merupakan aksara penutup kata, dan aksara mati ");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("\\");
                penjelasan.setText("Pangkon");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("afus\\");
                arti.setText("Adus");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("wedus\\");
                arti2.setText("Wedhus");
                break;
            case 36:
                filosofi.setText("Merupakan Pasangan Aksara Murda Ka, Bentuknya seperti ka tapi letaknya dibawah");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("¯");
                penjelasan.setText("Pasangan Murda Ka");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("r[fn¯bul\\");
                arti.setText("raden kabul");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("r[fn¯mus\\");
                arti2.setText("raden kamus");
                FloatingActionButton floatingActionButton1 = findViewById(R.id.fabback);
                floatingActionButton1.setVisibility(View.VISIBLE);
                floatingActionButton1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), isilanjutan.class);
                        intent.putExtra("aksara", 18);
                        startActivity(intent);
                    }
                });

//                FloatingActionButton fab3 = (FloatingActionButton) findViewById(R.id.fab);
//                fab3.setVisibility(fab3.VISIBLE);
//                fab3.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent i = new Intent(getApplicationContext(), isilanjutan.class);
//                        i.putExtra("aksara", 18);
//                        startActivity(i);
//                    }
//                });
                break;
            case 37:
                filosofi.setText("Sandangan pepet dipakai untuk melambangkan vokal \"e\" dalam suku kata \n" +
                        "Klik tombol suara jika bingung membedakan pengunaan pepet dan taling");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("e");
                penjelasan.setText("Sandangan e");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("se[go");
                arti.setText("sego");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("pecel\\");
                arti2.setText("pecel");
                TapTargetView.showFor(this,
                        TapTarget.forView(findViewById(R.id.fab1), "Suara", "Tekan tombol suara untuk mendengar penjelasan")
                                .tintTarget(false)
                                .outerCircleColor(R.color.slide1)
                                .textColor(R.color.white)
                                .cancelable(false));
                FloatingActionButton fab31 = findViewById(R.id.fab1);
                fab31.setVisibility(View.VISIBLE);
                fab31.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.lambang);
                        suarabenar.start();
                    }
                });
                break;

            case 38:
                filosofi.setText("Penulisan aksara angka harus diapit / dijepit dengan pada pangkat");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("1");
                penjelasan.setText("Aksara angka 1");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText(":2001:");
                arti.setText("2001");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("pilih:1:");
                arti2.setText("pilih 1");
                break;
            case 39:
                filosofi.setText("Penulisan aksara angka harus diapit / dijepit dengan pada pangkat");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("2");
                penjelasan.setText("Aksara Angka 2");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("[no[m/o:2:");
                arti.setText("Nomor 2");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText(":2018:");
                arti2.setText("2018");
                break;
            case 40:
                filosofi.setText("Penulisan aksara angka harus diapit / dijepit dengan pada pangkat");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("3");
                penjelasan.setText("Aksara Angka 3");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText(":3:sj");
                arti.setText("3 saja");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("sypuv:3:spu");
                arti2.setText("saya punya 3 sapu");
                break;
            case 41:
                filosofi.setText("Penulisan aksara angka harus diapit / dijepit dengan pada pangkat");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("4");
                penjelasan.setText("Aksara Angka 4");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText(":4:bu=kus\\");
                arti.setText("4 Bungkus");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText(":4:ribu");
                arti2.setText("4 Ribu");
                break;
            case 42:
                filosofi.setText("Penulisan aksara angka harus diapit / dijepit dengan pada pangkat");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("5");
                penjelasan.setText("Aksara Angka 5");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText(":5:ka[nT=o");
                arti.setText("5 Kantong");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText(":5:ka/fus\\");
                arti2.setText("5 kardus");
                break;
            case 43:
                filosofi.setText("Penulisan aksara angka harus diapit / dijepit dengan pada pangkat");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("6");
                penjelasan.setText("Aksara Angka 6");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText(":6:ari");
                arti.setText("6 hari");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText(":600:");
                arti2.setText("600");
                break;
            case 44:
                filosofi.setText("Penulisan aksara angka harus diapit / dijepit dengan pada pangkat");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("7");
                penjelasan.setText("Aksara Angka 7");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText(":7:jm\\");
                arti.setText("7 jam");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText(":7:menit\\");
                arti2.setText("7 Menit");
                break;
            case 45:
                filosofi.setText("Penulisan aksara angka harus diapit / dijepit dengan pada pangkat");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("8");
                penjelasan.setText("Aksara Angka 8");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText(":8:ki[lo");
                arti.setText("8 Kilo");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText(":8:taun\\");
                arti2.setText("8 Tahun");
                break;
            case 46:
                filosofi.setText("Penulisan aksara angka harus diapit / dijepit dengan pada pangkat");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("9");
                penjelasan.setText("Aksara Angka 9");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText(":9:buln\\");
                arti.setText("9 Bulan");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText(":9:taun\\");
                arti2.setText("9 Tahun");
                break;
            case 47:
                filosofi.setText("Penulisan aksara angka harus diapit / dijepit dengan pada pangkat");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("0");
                penjelasan.setText("Aksara Angka 0");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("biji:100:");
                arti.setText("biji 100");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText(":20:[w=o");
                arti2.setText("20 Wong");
                break;
            case 48:
                filosofi.setText("Merupakan Pasangan Aksara Murda Na, Bentuknya seperti ka tapi letaknya dibawah");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("®");
                penjelasan.setText("Pasangan Murda Na");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("r[fn®jib\\");
                arti.setText("raden najib");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("r[fn®tkusum");
                arti2.setText("raden natakusuma");
                FloatingActionButton floatingActionButton = findViewById(R.id.fabback);
                floatingActionButton.setVisibility(View.VISIBLE);
                floatingActionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), isilanjutan.class);
                        intent.putExtra("aksara", 17);
                        startActivity(intent);
                    }
                });
                break;
            case 49:
                filosofi.setText("Merupakan Pasangan Aksara Murda Ta, Bentuknya seperti Aksara murda sa tapi letaknya dibawah aksara yang dipasangkan");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("±");
                penjelasan.setText("Pasangan Murda Ta");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("r[f[n±oyib\\");
                arti.setText("raden toyib");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("r[fn±nJ|=");
                arti2.setText("raden tanjung");
                FloatingActionButton floatingActionButton11 = findViewById(R.id.fabback);
                floatingActionButton11.setVisibility(View.VISIBLE);
                floatingActionButton11.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), isilanjutan.class);
                        intent.putExtra("aksara", 19);
                        startActivity(intent);
                    }
                });
                break;
            case 50:
                filosofi.setText("Merupakan Pasangan Aksara Murda Sa \n\n" +
                        "Sebenarnya aksara Murda pasangan Sa disambung dengan aksara diatasnya.");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("°");
                penjelasan.setText("Pasangan Murda Sa");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("r[fn°b/");
                arti.setText("raden sabar");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("r[fn°ki/");
                arti2.setText("raden sakir");
                FloatingActionButton floatingActionButton12 = findViewById(R.id.fabback);
                floatingActionButton12.setVisibility(View.VISIBLE);
                floatingActionButton12.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), isilanjutan.class);
                        intent.putExtra("aksara", 20);
                        startActivity(intent);
                    }
                });
                break;
            case 51:
                filosofi.setText("Merupakan Pasangan Aksara Murda Pa \n\n" +
                        "Pa terletak di samping kiri aksara yang dipasangkan");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("²");
                penjelasan.setText("Pasangan Murda Pa");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("r[fn²ut]");
                arti.setText("raden putra");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("r[fn²[zrn\\");
                arti2.setText("raden pangeran");
                FloatingActionButton floatingActionButton13 = findViewById(R.id.fabback);
                floatingActionButton13.setVisibility(View.VISIBLE);
                floatingActionButton13.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), isilanjutan.class);
                        intent.putExtra("aksara", 21);
                        startActivity(intent);
                    }
                });
                break;
            case 52:
                filosofi.setText("Merupakan Pasangan Aksara Murda Nya \n\n" +
                        "Pasangannya bentuknya seperti murda nya namun letaknya dibawah aksara yang dipassangkan");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("³");
                penjelasan.setText("Pasangan Murda Nya");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("r[fn³wiji");
                arti.setText("raden nyawiji");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("r[f[n³ov");
                arti2.setText("raden nyonya");
                FloatingActionButton floatingActionButton14 = findViewById(R.id.fabback);
                floatingActionButton14.setVisibility(View.VISIBLE);
                floatingActionButton14.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), isilanjutan.class);
                        intent.putExtra("aksara", 22);
                        startActivity(intent);
                    }
                });
                break;
            case 53:
                filosofi.setText("Merupakan Pasangan Aksara Murda Ga \n\n" +
                        "Pasangannya bentuknya seperti murda Ga namun letaknya dibawah aksara yang dipassangkan");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("´");
                penjelasan.setText("Pasangan Murda Ga");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("r[fn´nfu=");
                arti.setText("raden gandung");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("r[fn´il=");
                arti2.setText("raden gilang");
                FloatingActionButton floatingActionButton15 = findViewById(R.id.fabback);
                floatingActionButton15.setVisibility(View.VISIBLE);
                floatingActionButton15.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), isilanjutan.class);
                        intent.putExtra("aksara", 23);
                        startActivity(intent);
                    }
                });
                break;
            case 54:
                filosofi.setText("Merupakan Pasangan Aksara Murda Ba \n\n" +
                        "Pasangannya bentuknya seperti murda Ba namun letaknya dibawah aksara yang dipassangkan");
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("µ");
                penjelasan.setText("Pasangan Murda Ba");
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("@ fesµlkn\\");
//                panelcontoh.setTextSize(27);
                arti.setText("kades balakan");
                panelcontoh2 = findViewById(R.id.panelcontoh2);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                arti2 = findViewById(R.id.arti2);
                panelcontoh2.setText("r[fnµgs\\");
                arti2.setText("raden bagas");
                FloatingActionButton floatingActionButton16 = findViewById(R.id.fabback);
                floatingActionButton16.setVisibility(View.VISIBLE);
                floatingActionButton16.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), isilanjutan.class);
                        intent.putExtra("aksara", 24);
                        startActivity(intent);
                    }
                });
                break;

        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayou1.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayou1.closeDrawer(GravityCompat.START);
        } else {
            Intent home = new Intent(getApplicationContext(), lanjutan.class);
            startActivity(home);
            super.onBackPressed();

        }


    }

}
