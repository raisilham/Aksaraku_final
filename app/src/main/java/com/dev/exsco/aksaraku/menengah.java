package com.dev.exsco.aksaraku;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class menengah extends AppCompatActivity {
    NavigationView navigationView1;
    DrawerLayout mDrawerLayou1;
    ActionBarDrawerToggle mToogle1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menengah);

        TextView gg = findViewById(R.id.ha);
        gg.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        gg.setText("H");
        TextView na = findViewById(R.id.na);
        na.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        na.setText("    N");
        TextView ca = findViewById(R.id.ca);
        ca.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ca.setText("    C");
        TextView ra = findViewById(R.id.ra);
        ra.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ra.setText("    R");
        ra.setTextSize(30);
        TextView ka = findViewById(R.id.ka);
        ka.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ka.setText("    K");
        TextView da = findViewById(R.id.da);
        da.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        da.setText("    F");
        TextView ta = findViewById(R.id.ta);
        ta.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ta.setText("    T");
        TextView sa = findViewById(R.id.sa);
        sa.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        sa.setText("S");
        TextView wa = findViewById(R.id.wa);
        wa.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        wa.setText("    W");
        TextView la = findViewById(R.id.la);
        la.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        la.setText("    L");
        TextView pa = findViewById(R.id.pa);
        pa.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        pa.setText("P");
        TextView dha = findViewById(R.id.dha);
        dha.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        dha.setText("   D");
        TextView ja = findViewById(R.id.ja);
        ja.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ja.setText("    J");
        TextView ya = findViewById(R.id.ya);
        ya.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ya.setText("    Y");
        TextView nya = findViewById(R.id.nya);
        nya.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        nya.setText("   V");
        TextView ma = findViewById(R.id.ma);
        ma.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ma.setText("    M");
        TextView ga = findViewById(R.id.ga);
        ga.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ga.setText("    G");
        ga.setTextSize(30);
        TextView ba = findViewById(R.id.ba);
        ba.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ba.setText("    B");
        TextView tha = findViewById(R.id.tha);
        tha.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        tha.setText("   Q");
        TextView nga = findViewById(R.id.nga);
        nga.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        nga.setText("   Z");
        nga.setTextSize(30);
        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        navigationView1 = findViewById(R.id.navigation);
        mDrawerLayou1 = findViewById(R.id.drawer_layout1);
        mToogle1 = new ActionBarDrawerToggle(this, mDrawerLayou1, toolbar, R.string.open, R.string.close);
        mDrawerLayou1.addDrawerListener(mToogle1);
        mDrawerLayou1.post(new Runnable() {
            @Override
            public void run() {
                mToogle1.syncState();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        CardView ha_i = findViewById(R.id.ha_i);
        ha_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 1);
                startActivity(i);
            }
        });
        CardView na_i = findViewById(R.id.na_i);
        na_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 2);
                startActivity(i);
            }
        });
        CardView ca_i = findViewById(R.id.ca_i);
        ca_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 3);
                startActivity(i);
            }
        });
        CardView ra_i = findViewById(R.id.ra_i);
        ra_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 4);
                startActivity(i);
            }
        });
        CardView ka_i = findViewById(R.id.ka_i);
        ka_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 5);
                startActivity(i);
            }
        });
        CardView da_i = findViewById(R.id.da_i);
        da_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 6);
                startActivity(i);
            }
        });
        CardView ta_i = findViewById(R.id.ta_i);
        ta_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 7);
                startActivity(i);
            }
        });
        CardView sa_i = findViewById(R.id.sa_i);
        sa_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 8);
                startActivity(i);
            }
        });
        CardView wa_i = findViewById(R.id.wa_i);
        wa_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 9);
                startActivity(i);
            }
        });
        CardView la_i = findViewById(R.id.la_i);
        la_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 10);
                startActivity(i);
            }
        });
        CardView pa_i = findViewById(R.id.pa_i);
        pa_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 11);
                startActivity(i);
            }
        });
        CardView dha_i = findViewById(R.id.dha_i);
        dha_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 12);
                startActivity(i);
            }
        });
        CardView ja_i = findViewById(R.id.ja_i);
        ja_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 13);
                startActivity(i);
            }
        });
        CardView ya_i = findViewById(R.id.ya_i);
        ya_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 14);
                startActivity(i);
            }
        });
        CardView nya_i = findViewById(R.id.nya_i);
        nya_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 15);
                startActivity(i);
            }
        });
        CardView ma_i = findViewById(R.id.ma_i);
        ma_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 16);
                startActivity(i);
            }
        });
        CardView ga_i = findViewById(R.id.ga_i);
        ga_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 17);
                startActivity(i);
            }
        });
        CardView ba_i = findViewById(R.id.ba_i);
        ba_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 18);
                startActivity(i);
            }
        });
        CardView tha_i = findViewById(R.id.tha_i);
        tha_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 19);
                startActivity(i);
            }
        });
        CardView nga_i = findViewById(R.id.nga_i);
        nga_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(menengah.this, sandangan.class);
                i.putExtra("aksara", 20);
                startActivity(i);
            }
        });
        CollapsingToolbarLayout collapsingToolbarLayout1 = findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout1.setTitle("Menengah");
        collapsingToolbarLayout1.setExpandedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout1.setCollapsedTitleTextColor(
                ContextCompat.getColor(this, R.color.white));
        collapsingToolbarLayout1.setExpandedTitleColor(
                ContextCompat.getColor(this, R.color.white));
        navigationView1.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.sejarah:
                        Intent i = new Intent(getApplicationContext(), sejarahh.class);
                        startActivity(i);
                        break;
                    case R.id.dasar:
                        Intent r = new Intent(getApplicationContext(), dasar.class);
                        startActivity(r);
                        break;
                    case R.id.menengah:
                        Toast.makeText(getApplicationContext(), "anda sedang berada di halaman menengah", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.lanjutan:
                        Intent t = new Intent(getApplicationContext(), lanjutan.class);
                        startActivity(t);
                        break;
                    case R.id.tebak:
                        Intent u = new Intent(getApplicationContext(), dashboard_lvltebak.class);
                        startActivity(u);
                        break;

                    case R.id.terjemah:
                        Intent w = new Intent(getApplicationContext(), dasboardterjemah.class);
                        startActivity(w);
                        break;
                    case R.id.about:
                        Intent ab = new Intent(getApplicationContext(), about.class);
                        startActivity(ab);
                        break;
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayou1.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayou1.closeDrawer(GravityCompat.START);
        } else {
            Intent home = new Intent(menengah.this, MainActivity.class);
            startActivity(home);
            super.onBackPressed();

        }

    }
}
