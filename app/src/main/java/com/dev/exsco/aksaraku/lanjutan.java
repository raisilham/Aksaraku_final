package com.dev.exsco.aksaraku;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class lanjutan extends AppCompatActivity {
    NavigationView navigationView1;
    DrawerLayout mDrawerLayou1;
    ActionBarDrawerToggle mToogle1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lanjutan);
        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        TextView pp = findViewById(R.id.pepett);
        pp.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        pp.setText("    e   ");
        TextView gg = findViewById(R.id.ha);
        gg.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        gg.setText("[   o");
        TextView na = findViewById(R.id.na);
        na.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        na.setText("    u   ");
        TextView ca = findViewById(R.id.ca);
        ca.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ca.setText("        i");
        TextView ra = findViewById(R.id.ra);
        ra.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ra.setText("[");
        TextView ka = findViewById(R.id.ka);
        ka.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ka.setText("    /   ");
        TextView da = findViewById(R.id.da);
        da.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        da.setText("   =    ");
        TextView ta = findViewById(R.id.ta);
        ta.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ta.setText("h");
        TextView sa = findViewById(R.id.sa);
        sa.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        sa.setText(",");
        TextView wa = findViewById(R.id.wa);
        wa.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        wa.setText(".");
        TextView la = findViewById(R.id.la);
        la.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        la.setText(";");
        TextView pa = findViewById(R.id.pa);
        pa.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        pa.setText("    ]   ");
        TextView dha = findViewById(R.id.dha);
        dha.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        dha.setText("    }  ");
        TextView ja = findViewById(R.id.ja);
        ja.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ja.setText("x");
        TextView ya = findViewById(R.id.ya);
        ya.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ya.setText("X");
        TextView nya = findViewById(R.id.nya);
        nya.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        nya.setText("?");
        TextView ma = findViewById(R.id.ma);
        ma.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ma.setText("    -");
        TextView pangkal = findViewById(R.id.pangkont);
        pangkal.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        pangkal.setText("    \\");
        TextView murda1 = findViewById(R.id.murda1);
        murda1.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        murda1.setText("!");
        TextView murda2 = findViewById(R.id.murda2);
        murda2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        murda2.setText("    @   ");
        TextView murda3 = findViewById(R.id.murda3);
        murda3.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        murda3.setText("#");
        TextView murda4 = findViewById(R.id.murda4);
        murda4.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        murda4.setText("$");
        TextView murda5 = findViewById(R.id.murda5);
        murda5.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        murda5.setText("%");
        TextView murda6 = findViewById(R.id.murda6);
        murda6.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        murda6.setText("^");
        TextView murda7 = findViewById(R.id.murda7);
        murda7.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        murda7.setText("&");
        TextView murda8 = findViewById(R.id.murda8);
        murda8.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        murda8.setText("*");
        TextView rekan1 = findViewById(R.id.rekan1);
        rekan1.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        rekan1.setText("k+");
        TextView rekan2 = findViewById(R.id.rekan2);
        rekan2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        rekan2.setText("p+");
        TextView rekan3 = findViewById(R.id.rekan3);
        rekan3.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        rekan3.setText("f+");
        TextView rekan4 = findViewById(R.id.rekan4);
        rekan4.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        rekan4.setText("g+");
        TextView rekan5 = findViewById(R.id.rekan5);
        rekan5.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        rekan5.setText("j+");
        TextView swara1 = findViewById(R.id.swara1);
        swara1.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        swara1.setText("A");
        TextView swara2 = findViewById(R.id.swara2);
        swara2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        swara2.setText("I");
        TextView swara3 = findViewById(R.id.swara3);
        swara3.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        swara3.setText("U");
        TextView swara4 = findViewById(R.id.swara4);
        swara4.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        swara4.setText("E");
        TextView swara5 = findViewById(R.id.swara5);
        swara5.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        swara5.setText("O");
        TextView angka1 = findViewById(R.id.angka1);
        angka1.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        angka1.setText("1");
        TextView angka2 = findViewById(R.id.angka2);
        angka2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        angka2.setText("2");
        TextView angka3 = findViewById(R.id.angka3);
        angka3.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        angka3.setText("3");
        TextView angka4 = findViewById(R.id.angka4);
        angka4.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        angka4.setText("4");
        TextView angka5 = findViewById(R.id.angka5);
        angka5.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        angka5.setText("5");
        TextView angka6 = findViewById(R.id.angka6);
        angka6.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        angka6.setText("6");
        TextView angka7 = findViewById(R.id.angka7);
        angka7.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        angka7.setText("7");
        TextView angka8 = findViewById(R.id.angka8);
        angka8.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        angka8.setText("8");
        TextView angka9 = findViewById(R.id.angka9);
        angka9.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        angka9.setText("9");
        TextView angka10 = findViewById(R.id.angka10);
        angka10.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        angka10.setText("0");
        navigationView1 = findViewById(R.id.navigation);
        mDrawerLayou1 = findViewById(R.id.drawer_layout1);
        mToogle1 = new ActionBarDrawerToggle(this, mDrawerLayou1, toolbar, R.string.open, R.string.close);
        mDrawerLayou1.addDrawerListener(mToogle1);
        mDrawerLayou1.post(new Runnable() {
            @Override
            public void run() {
                mToogle1.syncState();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        CollapsingToolbarLayout collapsingToolbarLayout1 = findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout1.setTitle("Lanjutan");
        collapsingToolbarLayout1.setExpandedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout1.setCollapsedTitleTextColor(
                ContextCompat.getColor(this, R.color.white));
        collapsingToolbarLayout1.setExpandedTitleColor(
                ContextCompat.getColor(this, R.color.white));
        CardView pepet = findViewById(R.id.pepet1);
        pepet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 37);
                startActivity(i);
            }
        });
        CardView ha_i = findViewById(R.id.ha_i);
        ha_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 1);
                startActivity(i);
            }
        });
        CardView na_i = findViewById(R.id.na_i);
        na_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 2);
                startActivity(i);
            }
        });
        CardView ca_i = findViewById(R.id.ca_i);
        ca_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 3);
                startActivity(i);
            }
        });
        CardView ra_i = findViewById(R.id.ra_i);
        ra_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 4);
                startActivity(i);
            }
        });
        CardView pangkal1 = findViewById(R.id.pangkon);
        pangkal1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 35);
                startActivity(i);
            }
        });
        CardView ka_i = findViewById(R.id.ka_i);
        ka_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 5);
                startActivity(i);
            }
        });
        CardView da_i = findViewById(R.id.da_i);
        da_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 6);
                startActivity(i);
            }
        });

        CardView ta_i = findViewById(R.id.ta_i);
        ta_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 7);
                startActivity(i);
            }
        });

        CardView sa_i = findViewById(R.id.sa_i);
        sa_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 8);
                startActivity(i);
            }
        });

        CardView wa_i = findViewById(R.id.wa_i);
        wa_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 9);
                startActivity(i);
            }
        });
        CardView la_i = findViewById(R.id.la_i);
        la_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 10);
                startActivity(i);
            }
        });
        CardView pa_i = findViewById(R.id.pa_i);
        pa_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 11);
                startActivity(i);
            }
        });
        CardView dha_i = findViewById(R.id.dha_i);
        dha_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 12);
                startActivity(i);
            }
        });
        CardView ja_i = findViewById(R.id.ja_i);
        ja_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 13);
                startActivity(i);
            }
        });
        CardView ya_i = findViewById(R.id.ya_i);
        ya_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 14);
                startActivity(i);
            }
        });
        CardView nya_i = findViewById(R.id.nya_i);
        nya_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 15);
                startActivity(i);
            }
        });
        CardView ma_i = findViewById(R.id.ma_i);
        ma_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 16);
                startActivity(i);
            }
        });
        // murda/////////////////////////////////////////////////
        CardView murda1_i = findViewById(R.id.murda1_i);
        murda1_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 17);
                startActivity(i);
            }
        });
        CardView murda2_i = findViewById(R.id.murda2_i);
        murda2_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 18);
                startActivity(i);
            }
        });
        CardView murda3_i = findViewById(R.id.murda3_i);
        murda3_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 19);
                startActivity(i);
            }
        });
        CardView murda4_i = findViewById(R.id.murda4_i);
        murda4_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 20);
                startActivity(i);
            }
        });
        CardView murda5_i = findViewById(R.id.murda5_i);
        murda5_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 21);
                startActivity(i);
            }
        });
        CardView murda6_i = findViewById(R.id.murda6_i);
        murda6_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 22);
                startActivity(i);
            }
        });
        CardView murda7_i = findViewById(R.id.murda7_i);
        murda7_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 23);
                startActivity(i);
            }
        });
        CardView murda8_i = findViewById(R.id.murda8_i);
        murda8_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 24);
                startActivity(i);
            }
        });
        //////////////////////////////aksara rekan////////////////
        CardView rekan_1 = findViewById(R.id.rekan_1);
        rekan_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 25);
                startActivity(i);
            }
        });
        CardView rekan_2 = findViewById(R.id.rekan_2);
        rekan_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 26);
                startActivity(i);
            }
        });
        CardView rekan_3 = findViewById(R.id.rekan_3);
        rekan_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 27);
                startActivity(i);
            }
        });
        CardView rekan_4 = findViewById(R.id.rekan_4);
        rekan_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 28);
                startActivity(i);
            }
        });
        CardView rekan_5 = findViewById(R.id.rekan_5);
        rekan_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 29);
                startActivity(i);
            }
        });
        CardView swara_1 = findViewById(R.id.swara_1);
        swara_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 30);
                startActivity(i);
            }
        });
        CardView swara_2 = findViewById(R.id.swara_2);
        swara_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 31);
                startActivity(i);
            }
        });
        CardView swara_3 = findViewById(R.id.swara_3);
        swara_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 32);
                startActivity(i);
            }
        });
        CardView swara_4 = findViewById(R.id.swara_4);
        swara_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 33);
                startActivity(i);
            }
        });
        CardView swara_5 = findViewById(R.id.swara_5);
        swara_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 34);
                startActivity(i);
            }
        });
        CardView angka11 = findViewById(R.id.angka1_i);
        angka11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 38);
                startActivity(i);
            }
        });
        CardView angka21 = findViewById(R.id.angka2_i);
        angka21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 39);
                startActivity(i);
            }
        });
        CardView angka31 = findViewById(R.id.angka3_I);
        angka31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 40);
                startActivity(i);
            }
        });
        CardView angka41 = findViewById(R.id.angka4_i);
        angka41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 41);
                startActivity(i);
            }
        });
        CardView angka51 = findViewById(R.id.angka5_i);
        angka51.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 42);
                startActivity(i);
            }
        });
        CardView angka61 = findViewById(R.id.angka6_i);
        angka61.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 43);
                startActivity(i);
            }
        });
        CardView angka71 = findViewById(R.id.angka7_i);
        angka71.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 44);
                startActivity(i);
            }
        });
        CardView angka82 = findViewById(R.id.angka8_i);
        angka82.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 45);
                startActivity(i);
            }
        });
        CardView angka91 = findViewById(R.id.angka9_i);
        angka91.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 46);
                startActivity(i);
            }
        });
        CardView angka101 = findViewById(R.id.angka10_i);
        angka101.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(lanjutan.this, isilanjutan.class);
                i.putExtra("aksara", 47);
                startActivity(i);
            }
        });
        navigationView1.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.sejarah:
                        Intent i = new Intent(getApplicationContext(), sejarahh.class);
                        startActivity(i);
                        break;
                    case R.id.dasar:
                        Intent r = new Intent(getApplicationContext(), dasar.class);
                        startActivity(r);
                        break;
                    case R.id.menengah:
                        Intent s = new Intent(getApplicationContext(), menengah.class);
                        startActivity(s);
                        break;
                    case R.id.lanjutan:
                        Toast.makeText(getApplicationContext(), "Anda sedang berada di halaman lannjutan", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.tebak:
                        Intent u = new Intent(getApplicationContext(), dashboard_lvltebak.class);
                        startActivity(u);
                        break;
                    case R.id.terjemah:
                        Intent w = new Intent(getApplicationContext(), dasboardterjemah.class);
                        startActivity(w);
                        break;
                    case R.id.about:
                        Intent ab = new Intent(getApplicationContext(), about.class);
                        startActivity(ab);
                        break;
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayou1.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayou1.closeDrawer(GravityCompat.START);
        } else {
            Intent home = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(home);
            super.onBackPressed();

        }

    }

}
