package com.dev.exsco.aksaraku;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class dashboard extends Fragment {

    public void onCreate(final Bundle savedInstanceState) {
        Log.d("buat", "onCreate: buat activity ");
        super.onCreate(savedInstanceState);



    }


    public dashboard() {

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // ubah kode ini menjadi View view
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        // gunakan view.findViewById
        CardView lanjutan = view.findViewById(R.id.lanjutan);
        CardView sejarah = view.findViewById(R.id.sejarah);
        CardView menengah = view.findViewById(R.id.menengah);
        CardView dasar = view.findViewById(R.id.dasar);
        dasar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dasar = new Intent(getActivity(), dasar.class);
                startActivity(dasar);
            }
        });

        sejarah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sejarah = new Intent(getActivity(), sejarahh.class);
                startActivity(sejarah);
            }
        });
        menengah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent menengah = new Intent(getActivity(), menengah.class);
                startActivity(menengah);
            }
        });
        lanjutan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // gunakan get Activity
                Intent lanjutan = new Intent(getActivity(), lanjutan.class);
                startActivity(lanjutan);
            }
        });
        return view;

    }
}

