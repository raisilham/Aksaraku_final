package com.dev.exsco.aksaraku;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class dashboard_lvltebak extends AppCompatActivity {
    MediaPlayer soundtrack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_lvltebak);
        Button lvl1 = findViewById(R.id.lvl1);
//        soundtrack = MediaPlayer.create(this, R.raw.soundtrack);
//        final MediaPlayer soundtrack = MediaPlayer.create(this, R.raw.soundtrack);
//        soundtrack.start();
        lvl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lvl1 = new Intent(getApplicationContext(), tebak.class);
                startActivity(lvl1);
            }
        });
        Button lvl2 = findViewById(R.id.lvl2);
        lvl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lvl2 = new Intent(getApplicationContext(), tebaklvl2.class);
                startActivity(lvl2);
            }
        });
        Button lvl3 = findViewById(R.id.lvl3);
        lvl3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lvl3 = new Intent(getApplicationContext(), tebaklvl3.class);
                startActivity(lvl3);
            }
        });
        Button lvl4 = findViewById(R.id.lvl4);
        lvl4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lvl4 = new Intent(getApplicationContext(), tebaklvl4.class);
                startActivity(lvl4);
            }
        });
    }

    @Override
    public void onBackPressed() {

        Intent home = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(home);
        super.onBackPressed();


    }
}
