package com.dev.exsco.aksaraku;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class hasil_lvl1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_lvl1);
//        TextView resultlabel = findViewById(R.id.iyh);
        TextView totalscore = findViewById(R.id.skor);
        TextView belajar = findViewById(R.id.belajar);
        int score = getIntent().getIntExtra("Benar", 0);
        totalscore.setText(score + "");

        if (score > 70) {
            final MediaPlayer suarasalah = MediaPlayer.create(this, R.raw.yay);
            suarasalah.setVolume(10, 10);
            suarasalah.start();
        } else {
            final MediaPlayer suarasalah = MediaPlayer.create(this, R.raw.faiz);
            suarasalah.setVolume(10, 10);
            belajar.setText("Belajar Lagi ya ");
            suarasalah.start();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Nilai kamu " + score + " loh");
            builder.setMessage("Semangat belajar dong\n" +
                    "kita harus terus melestarikan aksara jawa");
            builder.setPositiveButton("Iya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }
            });
            builder.show();


        }


    }


    public void kembali(View view) {
        Intent intent = new Intent(getApplicationContext(), dashboard_lvltebak.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intenthome = new Intent(getApplicationContext(), dashboard_lvltebak.class);
        startActivity(intenthome);
    }
}
