package com.dev.exsco.aksaraku;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class about extends AppCompatActivity {
    Animation downtoup, uptodown;
    ConstraintLayout C2;
    ImageView C1;

    @Override
    public void onBackPressed() {
//        C2.setAnimation(uptodown);
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        C2 = findViewById(R.id.cons);
        uptodown = AnimationUtils.loadAnimation(this, R.anim.uptodown);
        downtoup = AnimationUtils.loadAnimation(this, R.anim.downtoup);
        C2.setAnimation(downtoup);


    }


}
