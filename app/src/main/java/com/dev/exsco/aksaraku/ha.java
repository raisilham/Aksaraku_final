package com.dev.exsco.aksaraku;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;

import java.util.Objects;

public class ha extends AppCompatActivity {
    private int urutan = 1;
    private TextView penjelasan, big, filosofi, panelcontoh, arti, panelcontoh2, arti2;
    private NavigationView navigationView1;
    private DrawerLayout mDrawerLayou1;
    private ActionBarDrawerToggle mToogle1;
    CollapsingToolbarLayout collapsingToolbarLayout1;
    FloatingActionButton fab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ha);
        penjelasan = findViewById(R.id.penjelasan_ha);
        big = findViewById(R.id.big);
        filosofi = findViewById(R.id.filosofi);
        panelcontoh = findViewById(R.id.panelcontoh);
        arti = findViewById(R.id.arti);
        panelcontoh2 = findViewById(R.id.panelcontoh2);
        arti2 = findViewById(R.id.arti2);
        Bundle extras = getIntent().getExtras();
        urutan = Objects.requireNonNull(extras).getInt("aksara");
        aksaraSelector(urutan);
        Boolean firstLoad = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .getBoolean("firstLoad", true);
        if (firstLoad) {
            TapTargetView.showFor(this,
                    TapTarget.forView(findViewById(R.id.fab), "Tombol ini untuk melihat sandangan", "Untuk melihat sandhangan dari Aksara jawa itu")
                            .tintTarget(false)
                            .outerCircleColor(R.color.slide1)
                            .textColor(R.color.white)
                            .cancelable(false));
            TapTargetView.showFor(this,
                    TapTarget.forView(findViewById(R.id.fabsuara), "Tombol ini untuk Mendengar", "Untuk mendengarkan bunyi")
                            .tintTarget(false)
                            .outerCircleColor(R.color.slide2)
                            .textColor(R.color.white)
                            .cancelable(false));
//            getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit().putBoolean("firstLoad", false).apply();
            getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit().putBoolean("firstLoad", false).apply();
        }

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        navigationView1 = findViewById(R.id.navigation);
        mDrawerLayou1 = findViewById(R.id.drawer_layout1);
        mToogle1 = new ActionBarDrawerToggle(this, mDrawerLayou1, toolbar, R.string.open, R.string.close);
        mDrawerLayou1.addDrawerListener(mToogle1);
        mDrawerLayou1.post(new Runnable() {
            @Override
            public void run() {
                mToogle1.syncState();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        collapsingToolbarLayout1 = findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout1.setTitle("Dasar");
        collapsingToolbarLayout1.setExpandedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout1.setCollapsedTitleTextColor(
                ContextCompat.getColor(this, R.color.white));
        collapsingToolbarLayout1.setExpandedTitleColor(
                ContextCompat.getColor(this, R.color.white));
        navigationView1.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.sejarah:
                        Intent i = new Intent(getApplicationContext(), sejarahh.class);
                        startActivity(i);
                        break;
                    case R.id.dasar:
                        Intent r = new Intent(getApplicationContext(), dasar.class);
                        startActivity(r);
                        break;
                    case R.id.menengah:
                        Intent z = new Intent(getApplicationContext(), menengah.class);
                        startActivity(z);
                        break;
                    case R.id.lanjutan:
                        Intent t = new Intent(getApplicationContext(), lanjutan.class);
                        startActivity(t);
                        break;
                    case R.id.tebak:
                        Intent u = new Intent(getApplicationContext(), dashboard_lvltebak.class);
                        startActivity(u);
                        break;
                    case R.id.terjemah:
                        Intent w = new Intent(getApplicationContext(), dasboardterjemah.class);
                        startActivity(w);
                        break;
                    case R.id.about:
                        Intent ab = new Intent(getApplicationContext(), about.class);
                        startActivity(ab);
                        break;
                }
                return false;
            }
        });
    }

    private void aksaraSelector(int selector) {
        switch (selector) {
            case 1:
                filosofi.setText(R.string.filosofi_ha);
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("a");
                penjelasan.setText(R.string.ha);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText(R.string.contoh_ha);
                arti.setText(R.string.arti_ha);
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("afi/");
                arti2.setText("Hadir");
                FloatingActionButton fab = findViewById(R.id.fab);
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 1);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab110 = findViewById(R.id.fabsuara);
                fab110.bringToFront();
                fab110.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ha1);
                        suarabenar.start();
                    }
                });

                break;
            case 2:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("n");
                filosofi.setText(R.string.filosofi_na);
                penjelasan.setText(R.string.na);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("nsi");
                arti.setText("Nasi");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("nns\\");
                arti2.setText("Nanas");
                FloatingActionButton fab2 = findViewById(R.id.fab);
                fab2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 2);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab111 = findViewById(R.id.fabsuara);
                fab111.bringToFront();
                fab111.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.na);
                        suarabenar.start();
                    }
                });
                break;
            case 3:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("c");
                filosofi.setText(R.string.filosofi_ca);
                penjelasan.setText(R.string.ca);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText(R.string.carakan_ca);
                arti.setText("Carakan");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("cepkCepk\\");
                arti2.setText("Cepak cepak");
                FloatingActionButton fab3 = findViewById(R.id.fab);
                fab3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 3);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab112 = findViewById(R.id.fabsuara);
                fab112.bringToFront();
                fab112.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ca);
                        suarabenar.start();
                    }
                });
                break;
            case 4:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("r");
                filosofi.setText(R.string.filosofi_ra);
                penjelasan.setText(R.string.ra);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("rgi");
                arti.setText("Ragi");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("rugi");
                arti2.setText("Rugi");
                FloatingActionButton fab4 = findViewById(R.id.fab);
                fab4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 4);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab113 = findViewById(R.id.fabsuara);
                fab113.bringToFront();
                fab113.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ra);
                        suarabenar.start();
                    }
                });
                break;
            case 5:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("k");
                filosofi.setText(R.string.filosofi_ka);
                penjelasan.setText(R.string.ka);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("ksu/");
                arti.setText("Kasur");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("kij=");
                arti2.setText("Kijang");
                FloatingActionButton fab5 = findViewById(R.id.fab);
                fab5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 5);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab114 = findViewById(R.id.fabsuara);
                fab114.bringToFront();
                fab114.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ka);
                        suarabenar.start();
                    }
                });
                break;
            case 6:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("f");
                filosofi.setText(R.string.filosofi_da);
                penjelasan.setText(R.string.Da);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("ffi");
                arti.setText("Dadi");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("ffu");
                arti2.setText("Dadu");
                FloatingActionButton fab6 = findViewById(R.id.fab);
                fab6.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 6);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab115 = findViewById(R.id.fabsuara);
                fab115.bringToFront();
                fab115.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.da);
                        suarabenar.start();
                    }
                });
                break;
            case 7:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("t");
                filosofi.setText(R.string.filosofi_ta);
                penjelasan.setText(R.string.ta);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("tau");
                arti.setText("Tahu");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("tnh");
                arti2.setText("Tanah");
                FloatingActionButton fab7 = findViewById(R.id.fab);
                fab7.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 7);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab116 = findViewById(R.id.fabsuara);
                fab116.bringToFront();
                fab116.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Log.d("text", "trst");
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ta);
                        suarabenar.start();
                    }
                });
                break;
            case 8:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("s");
                filosofi.setText(R.string.filosofi_sa);
                penjelasan.setText(R.string.sa);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("spi");
                arti.setText("Sapi");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("susu");
                arti2.setText("Susu");
                FloatingActionButton fab8 = findViewById(R.id.fab);
                fab8.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 8);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab117 = findViewById(R.id.fabsuara);
                fab117.bringToFront();
                fab117.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.sa);
                        suarabenar.start();
                    }
                });
                break;

            case 9:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("w");
                filosofi.setText(R.string.filosofi_wa);
                penjelasan.setText(R.string.wa);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText(R.string.carakan_wa);
                arti.setText("Waton");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("wfh");
                arti2.setText("wadah");
                FloatingActionButton fab9 = findViewById(R.id.fab);
                fab9.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 9);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab118 = findViewById(R.id.fabsuara);
                fab118.bringToFront();
                fab118.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.wa);
                        suarabenar.start();
                    }
                });
                break;
            case 10:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("l");
                filosofi.setText(R.string.filosofi_la);
                penjelasan.setText(R.string.La);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("ln=");
                arti.setText("Lanang");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("lrvn\\");
                arti2.setText("larangan");
                FloatingActionButton fab10 = findViewById(R.id.fab);
                fab10.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 10);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab119 = findViewById(R.id.fabsuara);
                fab119.bringToFront();
                fab119.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.la);
                        suarabenar.start();
                    }
                });
                break;
            case 11:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("p");
                filosofi.setText(R.string.filosofi_pa);
                penjelasan.setText(R.string.Pa);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("pri");
                arti.setText("Pari");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("pg/");
                arti2.setText("Pagar");
                FloatingActionButton fab11 = findViewById(R.id.fab);
                fab11.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 11);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab120 = findViewById(R.id.fabsuara);
                fab120.bringToFront();
                fab120.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.pa);
                        suarabenar.start();
                    }
                });
                break;
            case 12:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("d");
                filosofi.setText(R.string.filosofi_dha);
                penjelasan.setText(R.string.Dha);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("da/");
                arti.setText("Dhahar");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("dnD=");
                arti2.setText("Dhandhang");
                FloatingActionButton fab12 = findViewById(R.id.fab);
                fab12.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 12);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab121 = findViewById(R.id.fabsuara);
                fab121.bringToFront();
                fab121.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.dha);
                        suarabenar.start();
                    }
                });
                break;
            case 13:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("j");
                filosofi.setText(R.string.filosofi_ja);
                penjelasan.setText(R.string.Ja);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("jy");
                arti.setText("jaya");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("jk");
                arti2.setText("Jaka");
                FloatingActionButton fab13 = findViewById(R.id.fab);
                fab13.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 13);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab122 = findViewById(R.id.fabsuara);
                fab122.bringToFront();
                fab122.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ja);
                        suarabenar.start();
                    }
                });
                break;
            case 14:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("y");
                filosofi.setText(R.string.filosofi_ya);
                penjelasan.setText(R.string.ya);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("ykin\\");
                arti.setText("yakin");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("y=");
                arti2.setText("yang");
                FloatingActionButton fab14 = findViewById(R.id.fab);
                fab14.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 14);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab123 = findViewById(R.id.fabsuara);
                fab123.bringToFront();
                fab123.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ya);
                        suarabenar.start();
                    }
                });
                break;
            case 15:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("v");
                filosofi.setText(R.string.filosofi_nya);
                penjelasan.setText(R.string.nya);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("vw=");
                arti.setText("Nyawang");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("vnÒ|ni");
                arti2.setText("nyantuni");
                FloatingActionButton fab15 = findViewById(R.id.fab);
                fab15.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 15);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab124 = findViewById(R.id.fabsuara);
                fab124.bringToFront();
                fab124.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.nya);
                        suarabenar.start();
                    }
                });
                break;
            case 16:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("m");
                filosofi.setText(R.string.filosofi_ma);
                penjelasan.setText(R.string.ma);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("md=");
                arti.setText("Madhang");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("mb/");
                arti2.setText("Mabar");
                FloatingActionButton fab16 = findViewById(R.id.fab);
                fab16.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 16);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab126 = findViewById(R.id.fabsuara);
                fab126.bringToFront();
                fab126.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ma);
                        suarabenar.start();
                    }
                });
                break;
            case 17:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("g");
                filosofi.setText(R.string.filosofi_ga);
                penjelasan.setText(R.string.ga);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("[gori");
                arti.setText("Gori");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("ggkGlk\\");
                arti2.setText("Gagak galak");
                FloatingActionButton fab17 = findViewById(R.id.fab);
                fab17.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 17);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab127 = findViewById(R.id.fabsuara);
                fab127.bringToFront();
                fab127.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ga);
                        suarabenar.start();
                    }
                });
                break;
            case 18:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("b");
                filosofi.setText(R.string.filosofi_ba);
                penjelasan.setText(R.string.ba);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("bt");
                arti.setText("Bata");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("bj");
                arti2.setText("Baja");
                FloatingActionButton fab18 = findViewById(R.id.fab);
                fab18.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 18);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab128 = findViewById(R.id.fabsuara);
                fab128.bringToFront();
                fab128.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.ba);
                        suarabenar.start();
                    }
                });
                break;
            case 19:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("q");
                filosofi.setText(R.string.filosofi_tha);
                penjelasan.setText(R.string.Tha);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("ceq");
                arti.setText("Cetha");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("qkQkn\\");
                arti2.setText("Thak Thakan");
                FloatingActionButton fab19 = findViewById(R.id.fab);
                fab19.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 19);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab129 = findViewById(R.id.fabsuara);
                fab129.bringToFront();
                fab129.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.tha);
                        suarabenar.start();
                    }
                });
                break;
            case 20:
                big.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                big.setText("z");
                filosofi.setText(R.string.filosofi_nga);
                penjelasan.setText(R.string.nga);
                panelcontoh.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh.setText("zji");
                arti.setText("Ngaji");
                panelcontoh2.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
                panelcontoh2.setText("zntuk\\");
                arti2.setText("Ngantuk");
                FloatingActionButton fab20 = findViewById(R.id.fab);
                fab20.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ha.this, sandangan.class);
                        i.putExtra("aksara", 20);
                        startActivity(i);
                    }
                });
                FloatingActionButton fab130 = findViewById(R.id.fabsuara);
                fab130.bringToFront();
                fab130.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final MediaPlayer suarabenar = MediaPlayer.create(getApplicationContext(), R.raw.nga);
                        suarabenar.start();
                    }
                });
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayou1.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayou1.closeDrawer(GravityCompat.START);
        } else {
            Intent home = new Intent(getApplicationContext(), dasar.class);
            startActivity(home);
            super.onBackPressed();

        }

    }

}
