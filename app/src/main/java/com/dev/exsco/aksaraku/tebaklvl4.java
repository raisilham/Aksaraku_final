package com.dev.exsco.aksaraku;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class tebaklvl4 extends AppCompatActivity {
    private TextView soal;
    private Button jawaban1;
    private Button jawaban2;
    private Button jawaban3;
    private Button jawaban4;

    private String jawabanbenar;
    private int nilai = 0;
    private int quizcount = 1;

    static final private int QUIZ_COUNT = 10;
    ArrayList<ArrayList<String>> quizarray = new ArrayList<>();
    String quizdata[][] = {


            {"x", "Pa Cerek", "Murda Ta", "Aksara murda Sa", "Aksara murda Pa"},
            {"X", "Nga lelet", "Aksara murda Nya", "Aksara murda Ga", "Aksara murda Ba"},
            {"k+", "Aksara Rekan Kha", "Aksara Rekan F/V", "Aksara Rekan Dz", "Aksara murda Ga"},
            {"p+", "Aksara Rekan F/V", "Aksara Rekan Kha", "Aksara Rekan Z", "Aksara Swara A"},
            {"j+", "Aksara Rekan Z", "Aksara murda Ga", "Aksara murda Nya", "Aksara murda Pa"},
            {"$", "Aksara murda Sa", "Aksara Murda Ta", "Aksara Murda Ka", "Murda Na"},
            {"@", "Aksara Murda Ka", "Aksara Murda Na", "Aksara Murda Ta", "Aksara murda Sa"},
            {"%", "Aksara murda Pa", "Aksara murda Sa", "Aksara Murda Ta", "Aksara Murda Ka"},
            {"!", "Aksara Murda Na", "pada pangkat ", "Aksara Murda Ka", "Aksara murda Nya"},
            {"^", "Aksara murda Nya", "Aksara murda Pa", "Aksara murda Sa", "Aksara Murda Ta"}
//             Pertanyaan, jawaban benar, piihan 1, pilihan 2, piliham 3
//             posisi pulihan sudah dirandom
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tebaklvl4);

        soal = findViewById(R.id.utama);
        jawaban1 = findViewById(R.id.pilihan4);
        jawaban2 = findViewById(R.id.pilihan3);
        jawaban3 = findViewById(R.id.pilihan2);
        jawaban4 = findViewById(R.id.pilihan1);

        for (int i = 0; i < quizdata.length; i++) {
            ArrayList<String> tmpArray = new ArrayList<>();
            tmpArray.add(quizdata[i][0]);
            tmpArray.add(quizdata[i][1]);
            tmpArray.add(quizdata[i][2]);
            tmpArray.add(quizdata[i][3]);
            tmpArray.add(quizdata[i][4]);
            quizarray.add(tmpArray);

        }
        selanjutnya();
    }

    public void selanjutnya() {
        Random random = new Random();
        int randomnum = random.nextInt(quizarray.size());

        ArrayList<String> quiz = quizarray.get(randomnum);
        soal.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        soal.setText(quiz.get(0));
        jawabanbenar = quiz.get(1);
        quiz.remove(0);
        Collections.shuffle(quiz);
        jawaban1.setText(quiz.get(0));
        jawaban2.setText(quiz.get(1));
        jawaban3.setText(quiz.get(2));
        jawaban4.setText(quiz.get(3));
        quizarray.remove(randomnum);
    }

    public void tebak(View view) {
        Button jawaban = findViewById(view.getId());
        String btn = jawaban.getText().toString();
        String peringatan;
        if (btn.equals(jawabanbenar)) {
            final MediaPlayer suarabenar = MediaPlayer.create(this, R.raw.benar);
            suarabenar.start();
            // jawaban benar
            nilai = nilai + 10;
            if (quizcount == QUIZ_COUNT) {
                Intent intent = new Intent(getApplicationContext(), hasil_lvl1.class);
                intent.putExtra("Benar", nilai);
                startActivity(intent);
            } else {
                quizcount++;
                selanjutnya();


            }
            final Toast toast = Toast.makeText(getApplicationContext(), "Jawaban mu benar", Toast.LENGTH_SHORT);
            toast.show();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toast.cancel();
                }
            }, 500);
//            Toast.makeText(getApplicationContext(), peringatan, Toast.LENGTH_SHORT).show();
//            Toast.makeText(getApplicationContext(), peringatan, Toast.LENGTH_SHORT).show();
        } else {
            final MediaPlayer suarasalah = MediaPlayer.create(this, R.raw.worng);
            suarasalah.start();
            peringatan = "Yahhh Jawabanmu salah";
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(peringatan);
            builder.setMessage("Jawaban yang benar tu " + jawabanbenar);
            builder.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (quizcount == QUIZ_COUNT) {
                        Intent intent = new Intent(getApplicationContext(), hasil_lvl1.class);
                        intent.putExtra("Benar", nilai);
                        startActivity(intent);
                    } else {
                        quizcount++;
                        selanjutnya();


                    }

                }
            });
            builder.setNegativeButton("Belajar lagi", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent belajar = new Intent(getApplicationContext(), lanjutan.class);
                    startActivity(belajar);
                }
            });
            builder.setCancelable(false);
            builder.show();

        }
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(getApplicationContext(), "selesaikan dulu sebisanya", Toast.LENGTH_LONG).show();
//            super.onBackPressed();
    }
}
