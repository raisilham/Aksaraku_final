package com.dev.exsco.aksaraku;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class sejarahh extends Activity {
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sejarahh);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());
        mViewPager = findViewById(R.id.container);
        mViewPager.setPageTransformer(true, new AnimasiDeep());
        mViewPager.setAdapter(mSectionsPagerAdapter);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sejarahh, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void kekanan(View view) {
        mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);

    }

    public void kekiri(View view) {

        mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
    }

//    public void kekanann(View view) {
//        mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
//    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = null;
            switch (getArguments().getInt(ARG_SECTION_NUMBER)) {
                case 1:
                    rootView = inflater.inflate(R.layout.fragment_sejarahh, container, false);

                    break;
                case 2:
                    rootView = inflater.inflate(R.layout.fragment_sejarahhh, container, false);
                    break;
                case 3:
                    rootView = inflater.inflate(R.layout.fragment_sejarahhhh, container, false);
                    break;
                case 4:
                    rootView = inflater.inflate(R.layout.fragment_sejarahhhhh, container, false);
                    break;
                case 5:
                    rootView = inflater.inflate(R.layout.fragment_sejarahhhhhh, container, false);
                    break;
                case 6:
                    rootView = inflater.inflate(R.layout.fragment_sejarahhhhhhh, container, false);
                    break;
                case 7:
                    rootView = inflater.inflate(R.layout.fragment_sejarahhhhhhhh, container, false);
                    break;
                case 8:
                    rootView = inflater.inflate(R.layout.fragment_sejarahhhhhhhhh, container, false);
                    break;
                case 9:
                    rootView = inflater.inflate(R.layout.fragment_sejarahhhhhhhhhh, container, false);
                    break;
                case 10:
                    rootView = inflater.inflate(R.layout.fragment_sejarahhhhhhhhhhh, container, false);
                    break;
                case 11:
                    rootView = inflater.inflate(R.layout.fragment_sejarahhhhhhhhhhhh, container, false);
                    break;
                case 12:
                    rootView = inflater.inflate(R.layout.fragment_sejarahhhhhhhhhhhhh, container, false);
                    break;
            }
//            View rootView = inflater.inflate(R.layout.fragment_sejarahh, container, false);
            return rootView;


        }

    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 12;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
                case 3:
                    return "SECTION 4";
                case 4:
                    return "SECTION 5";
                case 5:
                    return "SECTION 6";
                case 6:
                    return "SECTION 7";
                case 7:
                    return "SECTION 8";
                case 8:
                    return "SECTION 9";
                case 9:
                    return "SECTION 10";
                case 10:
                    return "SECTION 11";
                case 11:
                    return "SECTION 12";
            }
            return null;
        }
    }

//    @Override
//    protected void onPause() {
//
//        ring.stop();
//        super.onPause();
//    }

    @Override
    public void onBackPressed() {
        if (mViewPager.getCurrentItem() == 0) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);


//            super.onBackPressed();
        } else {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
        }

    }

/*
dasdasdasdas
 */
}
