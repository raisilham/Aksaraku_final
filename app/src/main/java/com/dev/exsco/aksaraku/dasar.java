package com.dev.exsco.aksaraku;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class dasar extends AppCompatActivity {
    NavigationView navigationView1;
    DrawerLayout mDrawerLayou1;
    ActionBarDrawerToggle mToogle1;
    MediaPlayer soundtrack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dasar);
        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);


        TextView gg = findViewById(R.id.ha);
        gg.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        gg.setText("a");
        TextView na = findViewById(R.id.na);
        na.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        na.setText("n");
        TextView ca = findViewById(R.id.ca);
        ca.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ca.setText("c");
        TextView ra = findViewById(R.id.ra);
        ra.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ra.setText("r");
        TextView ka = findViewById(R.id.ka);
        ka.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ka.setText("k");
        TextView da = findViewById(R.id.da);
        da.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        da.setText("f");
        TextView ta = findViewById(R.id.ta);
        ta.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ta.setText("t");
        TextView sa = findViewById(R.id.sa);
        sa.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        sa.setText("s");
        TextView wa = findViewById(R.id.wa);
        wa.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        wa.setText("w");
        TextView la = findViewById(R.id.la);
        la.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        la.setText("l");
        TextView pa = findViewById(R.id.pa);
        pa.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        pa.setText("p");
        TextView dha = findViewById(R.id.dha);
        dha.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        dha.setText("d");
        TextView ja = findViewById(R.id.ja);
        ja.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ja.setText("j");
        TextView ya = findViewById(R.id.ya);
        ya.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ya.setText("y");
        TextView nya = findViewById(R.id.nya);
        nya.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        nya.setText("v");
        TextView ma = findViewById(R.id.ma);
        ma.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ma.setText("m");
        TextView ga = findViewById(R.id.ga);
        ga.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ga.setText("g");
        TextView ba = findViewById(R.id.ba);
        ba.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        ba.setText("b");
        TextView tha = findViewById(R.id.tha);
        tha.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        tha.setText("q");
        TextView nga = findViewById(R.id.nga);
        nga.setTypeface(Typeface.createFromAsset(getAssets(), "aksara.ttf"));
        nga.setText("z");

        navigationView1 = findViewById(R.id.navigation);
        mDrawerLayou1 = findViewById(R.id.drawer_layout1);
        mToogle1 = new ActionBarDrawerToggle(this, mDrawerLayou1, toolbar, R.string.open, R.string.close);
        mDrawerLayou1.addDrawerListener(mToogle1);
        mDrawerLayou1.post(new Runnable() {
            @Override
            public void run() {
                mToogle1.syncState();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        CollapsingToolbarLayout collapsingToolbarLayout1 = findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout1.setTitle("Dasar");
        collapsingToolbarLayout1.setExpandedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout1.setCollapsedTitleTextColor(
                ContextCompat.getColor(this, R.color.white));
        collapsingToolbarLayout1.setExpandedTitleColor(
                ContextCompat.getColor(this, R.color.white));

        CardView ha_i = findViewById(R.id.ha_i);
        ha_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 1);
                startActivity(i);
            }
        });
        CardView na_i = findViewById(R.id.na_i);
        na_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 2);
                startActivity(i);
            }
        });
        CardView ca_i = findViewById(R.id.ca_i);
        ca_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 3);
                startActivity(i);
            }
        });
        CardView ra_i = findViewById(R.id.ra_i);
        ra_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 4);
                startActivity(i);
            }
        });
        CardView ka_i = findViewById(R.id.ka_i);
        ka_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 5);
                startActivity(i);
            }
        });

        CardView da_i = findViewById(R.id.da_i);
        da_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 6);
                startActivity(i);
            }
        });
        CardView ta_i = findViewById(R.id.ta_i);
        ta_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 7);
                startActivity(i);
            }
        });
        CardView sa_i = findViewById(R.id.sa_i);
        sa_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 8);
                startActivity(i);
            }
        });
        CardView wa_i = findViewById(R.id.wa_i);
        wa_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 9);
                startActivity(i);
            }
        });
        CardView la_i = findViewById(R.id.la_i);
        la_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 10);
                startActivity(i);
            }
        });
        CardView pa_i = findViewById(R.id.pa_i);
        pa_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 11);
                startActivity(i);
            }
        });
        CardView dha_i = findViewById(R.id.dha_i);
        dha_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 12);
                startActivity(i);
            }
        });
        CardView ja_i = findViewById(R.id.ja_i);
        ja_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 13);
                startActivity(i);
            }
        });
        CardView ya_i = findViewById(R.id.ya_i);
        ya_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 14);
                startActivity(i);
            }
        });
        CardView nya_i = findViewById(R.id.nya_i);
        nya_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 15);
                startActivity(i);
            }
        });
        CardView ma_i = findViewById(R.id.ma_i);
        ma_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 16);
                startActivity(i);
            }
        });
        CardView ga_i = findViewById(R.id.ga_i);
        ga_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 17);
                startActivity(i);
            }
        });
        CardView ba_i = findViewById(R.id.ba_i);
        ba_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 18);
                startActivity(i);
            }
        });
        CardView tha_i = findViewById(R.id.tha_i);
        tha_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 19);
                startActivity(i);
            }
        });
        CardView nga_i = findViewById(R.id.nga_i);
        nga_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(dasar.this, ha.class);
                i.putExtra("aksara", 20);
                startActivity(i);
            }
        });
//        CardView

        navigationView1.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.sejarah:
                        Intent i = new Intent(dasar.this, sejarahh.class);
                        startActivity(i);
                        break;
                    case R.id.dasar:
                        Toast.makeText(getApplicationContext(), "Anda sedang berada di halaman dasar", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.menengah:
                        Intent s = new Intent(dasar.this, menengah.class);
                        startActivity(s);
                        break;
                    case R.id.lanjutan:
                        Intent t = new Intent(dasar.this, lanjutan.class);
                        startActivity(t);
                        break;
                    case R.id.tebak:
                        Intent u = new Intent(dasar.this, dashboard_lvltebak.class);
                        startActivity(u);
                        break;

                    case R.id.terjemah:
                        Intent w = new Intent(dasar.this, dasboardterjemah.class);
                        startActivity(w);
                        break;
                    case R.id.about:
                        Intent ab = new Intent(getApplicationContext(), about.class);
                        startActivity(ab);
                        break;
                }
                return false;
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (mDrawerLayou1.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayou1.closeDrawer(GravityCompat.START);
        } else {

//            if (soundtrack.isPlaying()){
//                soundtrack.stop();
//            }
            Intent home = new Intent(dasar.this, MainActivity.class);
            startActivity(home);
            super.onBackPressed();


        }

    }
}

